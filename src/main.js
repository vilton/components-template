import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import vueSmoothScroll from 'vue-smoothscroll'

import UnderConstruction from './components/UnderConstruction.vue'
import HomePage from './components/HomePage.vue'

Vue.use(vueSmoothScroll);
Vue.use(VueRouter);

const router = new VueRouter({
  routes: [
    { path: '/under-construction', component: UnderConstruction },
    { path: '/', component: HomePage }
  ],
});

console.log('Router funcionando');


new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
